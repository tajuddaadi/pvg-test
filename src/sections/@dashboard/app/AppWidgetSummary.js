// @mui
import PropTypes from 'prop-types';
// import { alpha, styled } from '@mui/material/styles';
import { Card, Typography } from '@mui/material';
// components
// import Iconify from '../../../components/iconify';

// ----------------------------------------------------------------------

// const StyledIcon = styled('div')(({ theme }) => ({
//   margin: 'auto',
//   display: 'flex',
//   borderRadius: '50%',
//   alignItems: 'center',
//   width: theme.spacing(8),
//   height: theme.spacing(8),
//   justifyContent: 'center',
//   marginBottom: theme.spacing(3),
// }));

// ----------------------------------------------------------------------

AppWidgetSummary.propTypes = {
  color: PropTypes.string,
  icon: PropTypes.string,
  title: PropTypes.string.isRequired,
  total: PropTypes.string.isRequired,
  descriptions: PropTypes.string,
  sx: PropTypes.object,
};

export default function AppWidgetSummary({ title, total, descriptions, color = 'primary', sx, ...other }) {

  return (
    <Card
      sx={{
        padding: 2,
        textAlign: 'left',
        bgcolor: '#fff',
        boxShadow: '0px 0px 26px #F0F1FF',
        ...sx,
      }}
      {...other}
    >
      {/* <StyledIcon
        sx={{
          color: (theme) => theme.palette[color].dark,
          backgroundImage: (theme) =>
            `linear-gradient(135deg, ${alpha(theme.palette[color].dark, 0)} 0%, ${alpha(
              theme.palette[color].dark,
              0.24
            )} 100%)`,
        }}
      >
        <Iconify icon={icon} width={24} height={24} />
      </StyledIcon> */}

      <Typography variant="subtitle2" sx={{ fontSize: 16 }}>
        {title}
      </Typography>
      <Typography variant="subtitle2" sx={{ fontSize: 30 }}>{total}</Typography>
      <Typography variant="subtitle2" sx={{ textAlign: 'right', fontSize: 14, color: (theme) => theme.palette[color].main, }}>
        {descriptions}
      </Typography>
    </Card>
  );
}
