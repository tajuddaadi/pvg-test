// @mui
import PropTypes from 'prop-types';
import { Box, Card, Paper, Typography, CardHeader, CardContent } from '@mui/material';
// utils
import { fNumber } from '../../../utils/formatNumber';

// ----------------------------------------------------------------------

AppTopProduct.propTypes = {
  title: PropTypes.string,
  subheader: PropTypes.string,
  list: PropTypes.array.isRequired,
};

export default function AppTopProduct({ title, subheader, list, ...other }) {
  return (
    <Card sx={{ mt: 5 }} {...other}>
      <CardHeader title={title} subheader={subheader} />

      <CardContent>
        <Box
          sx={{
            display: 'grid',
            gap: 2,
            gridTemplateColumns: 'repeat(5, 1fr)',
          }}
        >
          {list.map((site) => (
            <Paper key={site.name} variant="outlined" sx={{ py: 1, textAlign: 'center', border: '3px solid #989EFF' }}>
              <Box component="img" alt={site.name} src={site.icon} sx={{ mb: 1, marginLeft: 'auto', marginRight: 'auto', height: 48, }} />
              <Typography variant="body2" sx={{ color: '#545DFF', fontSize: 16 }}>
                {site.name}
              </Typography>
              <Typography variant="h6" sx={{ color: '#7A7A7A', fontSize: 14 }}>{fNumber(site.value)}</Typography>
              <Typography variant="body2" sx={{ color: '#212121', fontSize: 18 }}>
                {site.transaction} transaction
              </Typography>
            </Paper>
          ))}
        </Box>
      </CardContent>
    </Card>
  );
}
