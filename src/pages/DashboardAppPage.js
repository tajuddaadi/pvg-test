import { Helmet } from 'react-helmet-async';
import { faker } from '@faker-js/faker';
// @mui
import { Grid, Container, Typography } from '@mui/material';
// sections
import {
  AppTopAgents,
  AppSalesChart,
  AppTopProduct,
  AppWidgetSummary,
} from '../sections/@dashboard/app';

// ----------------------------------------------------------------------

export default function DashboardAppPage() {
  return (
    <>
      <Helmet>
        <title> Dashboard </title>
      </Helmet>

      <Container maxWidth="xl" sx={{ mt: -4 }}>
        <Typography variant="h3" sx={{ fontSize: 30, fontWeight: 700, color: '#212121' }}>
          Dashboard
        </Typography>
        <Typography variant="h6" sx={{ mb: 10, fontSize: 18, fontWeight: 700, color: '#7A7A7A' }}>
          Today’s date: Sun, 10 April 2022
        </Typography>

        <Grid container spacing={3}>
          <Grid item xs={12} sm={6} md={4}>
            <AppWidgetSummary title="Profit" total="Rp 1.500.000" descriptions="0.3% compared to 7 days ago" color="error" icon={'ant-design:android-filled'} />
          </Grid>

          <Grid item xs={12} sm={6} md={4}>
            <AppWidgetSummary title="New Users" total="Rp 56.000.000" descriptions="0.5% compared to 7 days ago" color="success" icon={'ant-design:apple-filled'} />
          </Grid>

          <Grid item xs={12} sm={6} md={4}>
            <AppWidgetSummary title="Item Orders" total="1.090 transactions" descriptions="same as 7 days ago" color="warning" icon={'ant-design:arrow-up-outlined'} />
          </Grid>

          <Grid item xs={12} md={6} lg={8}>
            <AppSalesChart
              title="Sales Chart"
              chartLabels={[
                '01/01/2003',
                '02/01/2003',
                '03/01/2003',
                '04/01/2003',
                '05/01/2003',
                '06/01/2003',
                '07/01/2003',
                '08/01/2003',
                '09/01/2003',
                '10/01/2003',
                '11/01/2003',
              ]}
              chartData={[
                {
                  name: 'Team A',
                  type: 'area',
                  fill: 'gradient',
                  data: [4400000, 5500000, 4100000, 6700000, 2200000, 4300000, 2100000, 4100000, 5600000, 2700000, 4300000],
                },

              ]}
            />
            <AppTopProduct
              title="Top 5 Products"
              list={[
                {
                  name: 'Maxim',
                  value: 150000,
                  transaction: 120,
                  icon: "/assets/images/maxim.png",
                },
                {
                  name: 'Telkom',
                  value: 25000,
                  transaction: 98,
                  icon: "/assets/images/telkomsel.png",
                },
                {
                  name: 'Three',
                  value: 50000,
                  transaction: 74,
                  icon: "/assets/images/3.png",
                },
                {
                  name: 'OVO',
                  value: 100000,
                  transaction: 68,
                  icon: "/assets/images/ovo.png",
                },
                {
                  name: 'PLN Postpaid',
                  value: '',
                  transaction: 50,
                  icon: "/assets/images/pln.png",
                },
              ]}
            />
          </Grid>

          <Grid item xs={12} md={6} lg={4}>
            <AppTopAgents
              title="Top 5 Agents"
              list={[...Array(5)].map((_, index) => ({
                id: faker.datatype.uuid(),
                title: faker.name.jobTitle(),
                description: faker.name.jobTitle(),
                image: `/assets/images/covers/cover_${index + 1}.jpg`,
              }))}
            />
          </Grid>
        </Grid>
      </Container>
    </>
  );
}
